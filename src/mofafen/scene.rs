use eframe::egui::{self};
use egui::{Ui};

use super::types;

pub fn main_view(_my_app: &mut types::MyApp, ui: &mut Ui) {
    ui.vertical(|ui| {
        let _name_label = ui.label("*** Scene View ***");
        draw_scene_view(ui);
    });
}

fn draw_scene_view(ui: &mut Ui) {
    let mut im = egui::Margin::default();
    im.left = 9.0;
    im.right = 9.0;
    im.top = 4.0;
    im.bottom = 4.0;
    custom_window_frame(ui.ctx(), "egui with custom frame", |ui| {
        ui.label("This is just the contents of the window.");
        ui.horizontal(|ui| {
            ui.label("egui theme:");
            egui::widgets::global_dark_light_mode_buttons(ui);
        });
    });
}

fn custom_window_frame(ctx: &egui::Context, _title: &str, add_contents: impl FnOnce(&mut egui::Ui)) {
    use egui::*;

    let panel_frame = egui::Frame {
        fill: ctx.style().visuals.window_fill(),
        rounding: 10.0.into(),
        stroke: ctx.style().visuals.widgets.noninteractive.fg_stroke,
        outer_margin: 0.5.into(), // so the stroke is within the bounds
        ..Default::default()
    };

    CentralPanel::default().frame(panel_frame).show(ctx, |ui| {
        let app_rect = ui.max_rect();

        let title_bar_height = 400.0;
        let title_bar_width = 400.0;
        let title_bar_rect = {
            let mut rect = app_rect;
            rect.max.y = rect.min.y + title_bar_height;
            rect.max.x = rect.min.x + title_bar_width;
            rect
        };

        // Add the contents:
        let content_rect = {
            let mut rect = app_rect;
            rect.min.y = title_bar_rect.max.y;
            rect
        }
        .shrink(4.0);
        let mut content_ui = ui.child_ui(content_rect, *ui.layout());
        add_contents(&mut content_ui);
    });
}