use emath::align;
use egui_file::FileDialog;
use serde::{Serialize, Deserialize};
use std::{collections::HashMap, path::PathBuf};
use std::collections::HashSet;

use super::enums;

// The TileConfig structure abstracts the information needed to set a tile
// It uses a texture as a tilesheet, in which we need to know how many row and columns
// the texture is composed of.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TileConfig {
    pub id: u32,
    pub path: String,
    pub row_count: u8,
    pub col_count: u8,
    pub row_index : u8,
    pub col_index : u8,
    pub row_span : u8,
    pub col_span : u8
}

impl Default for TileConfig {
    fn default() -> Self {
        Self {
            id: 0,
            path : "tiles/effects_001.png".to_string(),
            row_count :0,
            col_count : 0, 
            row_index : 0,
            col_index : 0,
            row_span : 1, 
            col_span : 1
        }
    }
}

// The Root config is used as a root to export the tile config in a json file
#[derive(Serialize, Deserialize, Debug)]
pub struct RootConfig {
    #[serde(rename = "tile_configs")]
    pub tile_configs: HashMap<String, TileConfig>
}

pub struct DialogInfo {
    pub dialog: Option<FileDialog>,
    pub path: Option<PathBuf>,
}

pub struct InnerConfig {
    pub cur_working_dir: Option<PathBuf>
}

impl Default for InnerConfig {
    fn default() -> Self {
        let slf = Self {
            cur_working_dir : None
        };
        slf
    }
}

impl InnerConfig {
    #[allow(dead_code)]
    pub fn read_from_file(self: &mut Self) {
        // TODO
    }

}

pub struct MyApp {
    pub name: String,
    pub cur_name_tile_config : Option<String>,
    pub cur_tile_config : Option<TileConfig>,
    pub root_config : RootConfig,
    pub toasts : egui_toast::Toasts,
    pub opened_file: Option<PathBuf>,
    pub open_file_dialog: Option<FileDialog>,
    pub save_file_dialog: Option<FileDialog>,
    pub saved_file: Option<PathBuf>,
    pub open_dialog2 : DialogInfo,
    pub is_edit : bool,
    pub inner_config : InnerConfig,
    pub change_wd_file_dialog: Option<FileDialog>,
    pub editor_mode_enum : enums::EditorMode
}

impl Default for MyApp {
    fn default() -> Self {
        let mut slf = Self {
            name: "Mofafen config".to_owned(),
            cur_name_tile_config : Option::None,
            cur_tile_config : Option::None,
            root_config : RootConfig { tile_configs : HashMap::new()},
            toasts : egui_toast::Toasts::new().anchor(align::Align2::RIGHT_BOTTOM, (-10.0, -10.0)) // 10 units from the bottom right corner
            .direction(egui::Direction::BottomUp),
            opened_file : None,
            open_file_dialog : None,
            saved_file : None,
            save_file_dialog : None,
            open_dialog2 : DialogInfo{dialog: None, path: None},
            is_edit: false,
            inner_config : InnerConfig::default(),
            change_wd_file_dialog : None,
            editor_mode_enum : enums::EditorMode::TileConfig
        };

        // Read config from json file
        let input_filename = String::from("input.json");
        let serialized = std::fs::read_to_string(&input_filename).unwrap();
        let deserialized: RootConfig = serde_json::from_str(&serialized).unwrap();
        slf.root_config.tile_configs.clear();
        slf.root_config.tile_configs = deserialized.tile_configs;
        slf
    }
}

impl MyApp {

    // Open an existing config file, provided a correct path
    pub fn open(self: &mut Self, file_to_open : &PathBuf) {

        let file_content_as_string = std::fs::read_to_string(&file_to_open);
        match file_content_as_string {
            Ok(content) => {
                let deserialized  = serde_json::from_str::<RootConfig>(&content);
                match deserialized {
                    Ok(res) => {
                        let mut m_str= String::from("");
                        m_str.push_str("Opened file : ");
                        m_str.push_str(self.opened_file.as_ref().unwrap().file_name().unwrap().to_str().unwrap());
                        self.root_config.tile_configs.clear();
                        self.root_config.tile_configs = res.tile_configs;
                        self.toasts.add(egui_toast::Toast {
                            text:m_str.into() ,
                            kind: egui_toast::ToastKind::Success,
                            options: egui_toast::ToastOptions::default()
                                .duration_in_seconds(2.0)
                                .show_progress(true)
                        }); 
                    },
                    Err(_) => {
                        self.toasts.add(egui_toast::Toast {
                            text:"Cannot open the file".into() ,
                            kind: egui_toast::ToastKind::Error,
                            options: egui_toast::ToastOptions::default()
                                .duration_in_seconds(2.0)
                                .show_progress(true)
                        }); 
                    }
                }
                
            },
            Err(err) =>  {
                println!("Error : {}", err.kind());
                self.toasts.add(egui_toast::Toast {
                    text:"Cannot open the file".into() ,
                    kind: egui_toast::ToastKind::Error,
                    options: egui_toast::ToastOptions::default()
                        .duration_in_seconds(2.0)
                        .show_progress(true)
                }); 
            }

        }
        self.opened_file = None;
        
    }

    // Save the current config at the provided path
    pub fn save_as(self: &mut Self, file_to_save_into : &PathBuf) {

        let a = &self.root_config;
        println!("QCR --> file = {}", file_to_save_into.to_str().unwrap());
        match serde_json::to_string_pretty(&a) {
            Err(_) => println!("Failed to serialize target file"),
            Ok(res) => { 
                match std::fs::write(file_to_save_into, res) {
                    Err(err) => {
                        println!("Error := {}", err.to_string());
                        self.toasts.add(egui_toast::Toast {
                            text:"Cannot open the file".into() ,
                            kind: egui_toast::ToastKind::Error,
                            options: egui_toast::ToastOptions::default()
                                .duration_in_seconds(2.0)
                                .show_progress(true)
                        }); 
                    },
                    Ok(_) => {
                        self.toasts.add(egui_toast::Toast {
                            text:"Config has been saved successfully".into() ,
                            kind: egui_toast::ToastKind::Success,
                            options: egui_toast::ToastOptions::default()
                                .duration_in_seconds(2.0)
                                .show_progress(true)
                        }); 
                    }
                }
            }
        }
        self.saved_file = None;
        
    }

    fn get_id_set(self: &Self) -> HashSet<u32>{
        let mut v = HashSet::new();
        for tc in self.root_config.tile_configs.iter() {
            v.insert(tc.1.id);
        }
        v
    }

    pub fn get_new_id(self: &Self) -> Result<u32, String> {
        let used_ids = self.get_id_set();
        use rand::prelude::*;
        let mut rng = rand::thread_rng();
        let mut nums: Vec<u32> = (1..10).collect();
        nums.retain_mut(|x| !used_ids.contains(&x));
        nums.shuffle(&mut rng);
        match nums.first() {
            Some(v) => Ok(*v),
            None => Err(String::from("Couldn't find available id"))
        }
    }

    pub fn is_id_already_used(self: &Self, id: u32) -> bool {
        let id_set = self.get_id_set();
        id_set.contains(&id)
    }

    pub fn is_name_already_used(self: &Self, id: u32, name: String) -> bool{
        for tc in self.root_config.tile_configs.iter() {
            if tc.0.eq(&name) && tc.1.id != id {
                return true
            }
        }
        false
    }

}

pub enum ControlAction {
    Modify,
    Remove
}
