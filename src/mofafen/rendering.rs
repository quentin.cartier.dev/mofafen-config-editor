use std::{borrow::BorrowMut, ops::RangeInclusive};

use egui::{Color32, InnerResponse, Rounding, Separator, Stroke, Ui, Vec2};
use super::types;

/*
    - Render a preview of the file used for the tile config
    - Display the selected part inside the image
*/
pub fn render_preview(ui: &mut Ui, tile_config: &types::TileConfig, tile_file_path: &str) {
    ui.vertical(|ui| {
                        
                let response = ui.add_sized(Vec2::new(150., 150.), egui::Image::from_uri("file://assets/".to_owned() + tile_file_path));

                let preview_rect = response.rect.clone();
                let painter = ui.painter_at(preview_rect);
                painter.rect_stroke(preview_rect, Rounding::default(), Stroke::new(2., Color32::LIGHT_BLUE));

                //let response = preview_image.show_max_size(ui, preview_rect.max.to_vec2());
                let painter = ui.painter_at(preview_rect);

                // Rect of the area selected in the image ( the selected tile or tile span)
                let mut rect_selected = preview_rect.clone();
                let w = preview_rect.width() / tile_config.col_count as f32;
                let h = preview_rect.height() / tile_config.row_count as f32;

                let center = egui::Pos2::new(
                    preview_rect.left()
                        + w * (tile_config.col_index) as f32
                        + (w * tile_config.col_span as f32) / 2.0,
                        preview_rect.top()
                        + h * (tile_config.row_index) as f32
                        + (h * tile_config.row_span as f32) / 2.0,
                );

                rect_selected.set_width(w * tile_config.col_span as f32 - 2.0);
                rect_selected.set_height(h * tile_config.row_span as f32 - 2.0);
                rect_selected.set_center(center);

                //Show rectangle of selected area in the texture file
                let stroke = egui::Stroke::new(2.0, Color32::RED);
                painter.rect_stroke(rect_selected, egui::Rounding::default().at_least(0.0), stroke);
            //ui.painter_at(inner_resp.response.rect).rect_filled(c, egui::Rounding::default().at_least(10.), Color32::RED.gamma_multiply(0.2));
    });
}

/* 
    Display the control panel in the config card, where we have:
    - Modify button
    - Remove button
*/
pub fn render_tile_config_control_panel_view(
    ui: &mut Ui,
    tile_config: &types::TileConfig,
    has_already_config_being_modified: bool,
) -> (InnerResponse<()>, Option<types::ControlAction>) {
    let popup_id = ui.make_persistent_id(tile_config.id.to_owned());
    let mut resut_action: Option<types::ControlAction> = None;

    let inn_response = ui.vertical(|ui| {
        // Display preview of the tile selected
        ui.add_space(0.0);
        let btn_modify = ui.add(egui::widgets::Button::new("Modify"));
        // Asking to modify the tile info
        if btn_modify.clicked() {
            if has_already_config_being_modified {
                ui.memory_mut(|mem| mem.toggle_popup(popup_id));
                println!("Wrong Clicked");
            } else {
                resut_action = Some(types::ControlAction::Modify);
            }
        }

        if ui.add(egui::widgets::Button::new("Remove")).clicked() {
            if has_already_config_being_modified {
                resut_action = Some(types::ControlAction::Remove);
            }

        }

        // Popup
        let below = egui::AboveOrBelow::Below;
        egui::popup::popup_above_or_below_widget(ui, popup_id, &btn_modify, below, |ui| {
            ui.set_min_width(200.0);
            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    ui.label("Another tile config is currently under modification");
                    let close_btn = ui.add(egui::widgets::Button::new("Close"));

                    ui.add(Separator::default());

                    if close_btn.clicked() {
                        ui.memory_mut(|mem| mem.toggle_popup(popup_id));
                    }
                });
            });
        });
    });

    (inn_response, resut_action)
}

/*
    Render a card showing the information of a given tile config
    This card is an entry in the list of config items
*/
pub fn render_tile_info_content_view (
    ui: &mut Ui,
    tile_config: &types::TileConfig,
    tile_config_key: &str,
) -> egui::InnerResponse<()> {
    let old_spacing = ui.spacing().item_spacing.clone();
    ui.spacing_mut().item_spacing = Vec2::new(1., 1.);
    let response = ui.vertical(|ui| {

        let enrich = egui::RichText::new("Config Name")
            .background_color(Color32::LIGHT_BLUE.gamma_multiply(0.1))
            .italics()
            .monospace();

        ui.add(egui::widgets::Label::new(
            enrich
        ));
        ui.horizontal(|ui| {
                       
            add_field(ui.borrow_mut(), "ID", tile_config.id.to_string().as_str());
            add_field(ui.borrow_mut(), "Key name", tile_config_key.to_string().as_str());
            add_field(ui.borrow_mut(), "Filename", tile_config.path.to_string().as_str());
        });

        ui.horizontal(|ui| {
            
            add_field(ui.borrow_mut(), "Row Idx", tile_config.row_index.to_string().as_str());
            add_field(ui.borrow_mut(), "Col Idx", tile_config.col_index.to_string().as_str());
            add_field(ui.borrow_mut(), "Row Span", tile_config.row_span.to_string().as_str());
            add_field(ui.borrow_mut(), "Col Span", tile_config.row_span.to_string().as_str());
            add_field(ui.borrow_mut(), "Row Cnt", tile_config.row_count.to_string().as_str());
            add_field(ui.borrow_mut(), "Col Cnt", tile_config.col_count.to_string().as_str());
        });
    });// layout vertical
    ui.spacing_mut().item_spacing = old_spacing;
    ui.painter_at(response.response.rect).rect_stroke(
        response.response.rect,
        Rounding::default().at_least(3.).at_most(3.),
        egui::Stroke::new(3., Color32::GOLD.gamma_multiply(0.1)));
    
    response
}


/* 
    Display a field:
        - Field name
        - Field value
*/
fn add_field(ui :  &mut Ui, field_name : &str, field_value : &str) -> InnerResponse<()>{

    let frame = egui::Frame::none()
    .rounding(egui::Rounding::default().at_most(1.0).at_least(1.0))
    .stroke(egui::Stroke::new(1.0, Color32::GOLD.gamma_multiply(0.2)))
    .show(ui, |ui| {
        ui.add_space(1.);
        ui.vertical(|ui| {
            ui.set_min_width(10.);
            ui.label(
                egui::RichText::new(field_name)
                    .text_style(egui::style::TextStyle::Small)
                    .italics()
                    .color(Color32::DARK_GRAY)
            );
            ui.label(field_value);
        });
        ui.add_space(1.);
        
    });
    frame
}

pub fn add_slider<Num: emath::Numeric>(ui : &mut Ui, field_name : &str, value : &mut Num, range: RangeInclusive<Num>) -> InnerResponse<()> {
    let frame = egui::Frame::none()
    .rounding(egui::Rounding::default().at_most(1.0).at_least(1.0))
    .stroke(egui::Stroke::new(1.0, Color32::GOLD.gamma_multiply(0.2)))
    .show(ui, |ui| {
            ui.vertical(|ui| {
                ui.label(field_name);
                ui.add(egui::widgets::Slider::new(
                    value,
                    range
                ));
            });
    });
    frame
}   