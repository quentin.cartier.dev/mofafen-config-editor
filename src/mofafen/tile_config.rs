use egui::{Align2, Button, Color32, ScrollArea, Ui, Vec2};
use egui_file::FileDialog;
use std::env;
use std::path::PathBuf;

use super::rendering;
use super::types;

use super::types::TileConfig;

pub fn main_view(my_app: &mut types::MyApp, ui: &mut Ui) {

    ui.vertical(|ui| {
        if my_app.cur_tile_config.is_some() {
            /*
                Display the current tile config selected
            */

            let name: String = my_app.cur_name_tile_config.as_ref().unwrap().clone();
            let mut current_info = my_app.cur_tile_config.to_owned().unwrap();
            let mut name_key = my_app.cur_name_tile_config.as_ref().unwrap().clone();

            // Try render the editor of tile config
            let edit_return: Option<bool> = render_edit_tile_config_card(
                my_app,
                &mut current_info,
                ui,
                &mut name_key,
                my_app.is_edit,
            );
            my_app.cur_name_tile_config = Some(name_key);

            //
            if edit_return.is_some() {
                if edit_return.unwrap() {
                    my_app.toasts.add(egui_toast::Toast {
                        text: "Config applied".into(),
                        kind: egui_toast::ToastKind::Success,
                        options: egui_toast::ToastOptions::default()
                            .duration_in_seconds(2.0)
                            .show_progress(true),
                    });

                    // Add or update entry in the map of configs
                    let tile = my_app.cur_tile_config.to_owned().unwrap();
                    my_app.root_config.tile_configs.insert(name, tile);

                    my_app.cur_tile_config = None;
                    my_app.cur_name_tile_config = None;
                } else {
                    my_app.toasts.add(egui_toast::Toast {
                        text: "Config changes have been dropped".into(),
                        kind: egui_toast::ToastKind::Info,
                        options: egui_toast::ToastOptions::default()
                            .duration_in_seconds(2.0)
                            .show_progress(true),
                    });
                    my_app.cur_tile_config = None;
                    my_app.cur_name_tile_config = None;
                }
            }

            if my_app.cur_tile_config.is_some() {
                // Update the cur_tile
                my_app.cur_tile_config = Some(current_info);
            }
        } else {
            /*
                Display top panel to add config when no config is currently selected
            */

            ui.horizontal(|ui| {
                let _name_label = ui.label("Add Config : ");
                let btn = ui.add(egui::Button::new("+"));
                if btn.clicked() {
                    match my_app.get_new_id() {
                        Ok(new_id) => {
                            my_app.cur_tile_config = Some(TileConfig::default());
                            my_app.cur_name_tile_config = Some(String::from("DefaultName"));
                            my_app.cur_tile_config.as_mut().unwrap().id = new_id;
                            my_app.is_edit = false;
                        }
                        Err(err) => {
                            my_app.toasts.add(egui_toast::Toast {
                                text: err.into(),
                                kind: egui_toast::ToastKind::Error,
                                options: egui_toast::ToastOptions::default()
                                    .duration_in_seconds(2.0)
                                    .show_progress(true),
                            });
                        }
                    }
                }
            });
        }

        ui.label(
            egui::RichText::new("Tile config list")
                .text_style(egui::style::TextStyle::Monospace)
                .italics()
                .strong()
                .background_color(Color32::DARK_BLUE),
        );

        /*
           List of tile configs
        */
        let avail_height_for_list = ui.available_height() - 20.;
        let _config_list_response = ui.horizontal(|ui| {
            ui.set_min_height(50.);
            ui.set_max_height(avail_height_for_list);
            let mut im = egui::Margin::default();
            im.right = 15.0; // let some place for the scroll bar to not interfere with stroke
            im.top = 7.0;
            im.bottom = 7.0;

            let clip = ui.clip_rect();
            let m_max_width = clip.width();

            let color = Color32::LIGHT_YELLOW.gamma_multiply(0.1);
            let _frame_response = egui::Frame::none()
                .rounding(egui::Rounding::default().at_most(15.0).at_least(10.0))
                .inner_margin(im)
                .stroke(egui::Stroke::new(2.0, color))
                .show(ui, |ui| {
                    ui.set_max_width(m_max_width);

                    let scroll_area = ScrollArea::vertical().min_scrolled_width(100.0);

                    scroll_area.show(ui, |ui| {
                        ui.vertical(|ui| {
                            for (key, tile_info) in &my_app.root_config.tile_configs {
                                let option_cur_config = &my_app.cur_tile_config;

                                match draw_tile_config_view(
                                    ui,
                                    key.as_str(),
                                    tile_info,
                                    &tile_info.path,
                                    option_cur_config.as_ref(),
                                ) {
                                    None => {}
                                    Some(res) => match &my_app.cur_tile_config {
                                        Some(_cur_tile_config) => {}
                                        None => {
                                            my_app.cur_tile_config = Option::Some(res);
                                            my_app.cur_name_tile_config = Some(key.clone());
                                            my_app.is_edit = true;
                                        }
                                    },
                                }
                            }
                        });
                    }); //scroll area
                });
        });
    });
}

/**
 * Draw the list of tile config
 */
fn draw_tile_config_view(
    ui: &mut Ui,
    tile_config_key: &str,
    tile_config: &types::TileConfig,
    tile_file_path: &str,
    cur_selected_config: Option<&types::TileConfig>,
) -> Option<types::TileConfig> {
    let c = ui.clip_rect();
    //ui.painter_at(c).rect_filled(
    //    c,
    //    egui::Rounding::default().at_least(10.),
    //    Color32::GREEN.gamma_multiply(0.2),
    //);
    ui.set_max_width(c.width());
    // This variable is None, unless a tile config is selected for modification
    let mut tile_config_to_modify: Option<types::TileConfig> = Option::None;
    let mut im = egui::Margin::default();
    im.left = 9.0;
    im.right = 9.0;
    im.top = 4.0;
    im.bottom = 4.0;

    egui::Frame::none()
        .rounding(egui::Rounding::default().at_most(8.0).at_least(5.0))
        .inner_margin(im)
        .show(ui, |ui| {
            ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
                ui.set_max_width(c.width());
                let _layer1_inn_resp =
                    rendering::render_tile_info_content_view(ui, tile_config, tile_config_key);

                let (_layer2_inn_rest, result_control) =
                    rendering::render_tile_config_control_panel_view(
                        ui,
                        tile_config,
                        cur_selected_config.is_some(),
                    );
                

                match result_control {
                    Some(action) => match action {
                        types::ControlAction::Modify => {
                            let res = types::TileConfig::clone(tile_config);
                            tile_config_to_modify = Some(res);
                        }
                        types::ControlAction::Remove => {
                            tile_config_to_modify = None;
                        }
                    },
                    None => {
                        tile_config_to_modify = None;
                    }
                }

                rendering::render_preview(ui, tile_config, &tile_file_path);
            });
        });

    // return tile info if selected to modification
    if tile_config_to_modify.is_some() {
        Option::Some(tile_config_to_modify.unwrap())
    } else {
        Option::None
    }
}

/*
    This is the view displayed at top to modify a tile config
*/
fn render_edit_tile_config_card(
    my_app: &mut types::MyApp,
    target_tile: &mut types::TileConfig,
    ui: &mut Ui,
    target_tile_name: &mut String,
    is_edit: bool,
) -> Option<bool> {
    let mut optional_ret: Option<bool> = Option::None;

    // Display the tile config is there is currently one for modification
    let filename = my_app.cur_tile_config.as_ref().unwrap().path.clone();

    // Show tile configs
    if target_tile.row_count < 1 {
        target_tile.row_count = 1;
    }
    if target_tile.col_count < 1 {
        target_tile.col_count = 1;
    }

    ui.horizontal(|ui| {
        ui.vertical(|ui| {

            ui.vertical(|ui| {
                let name_label = ui.label(
                    egui::RichText::new("Tile config key: ")
                        .text_style(egui::style::TextStyle::Small)
                        .italics(),
                );
                if is_edit {
                    ui.colored_label(Color32::GOLD, target_tile_name)
                        .labelled_by(name_label.id);
                } else {
                    ui.text_edit_singleline(target_tile_name)
                        .labelled_by(name_label.id);
                }
            });


            ui.vertical(|ui| {
                let name_label = ui.label(
                    egui::RichText::new("Tile config id (int): ")
                        .text_style(egui::style::TextStyle::Small)
                        .italics(),
                );
                let mut m_id = target_tile.id.to_string();
                if is_edit {
                    ui.colored_label(Color32::GOLD, &mut m_id)
                        .labelled_by(name_label.id);
                } else {
                    ui.text_edit_singleline(&mut m_id)
                            .labelled_by(name_label.id);
                    if ui.add(egui::widgets::Button::new("Generate ID")).clicked() {
                        match my_app.get_new_id() {
                            Ok(new_id) => m_id = new_id.to_string(),
                            Err(_) => {}
                        }
                    }
                }
                match m_id.parse::<u32>() {
                    Ok(v) => {
                        target_tile.id = v;
                    }
                    Err(_) => {
                        // Not a number, ignore input
                    }
                }
            });

            ui.horizontal(|ui| {
                //ui.add(egui::Separator::default());
                ui.vertical(|ui| {
                    let name_label = ui.label(
                        egui::RichText::new("Path / filename: ")
                            .text_style(egui::style::TextStyle::Small)
                            .italics(),
                    );
                    let file_to_choose_resp = ui
                        .text_edit_singleline(&mut target_tile.path)
                        .labelled_by(name_label.id);
                    let dialog_info = &mut my_app.open_dialog2;
                    if file_to_choose_resp.clicked() {
                        let res = env::current_dir().unwrap();
                        let test = target_tile.path.clone();

                        let path = PathBuf::from(String::from(
                            res.into_os_string().into_string().unwrap()
                                + "/assets/"
                                + test.into_boxed_str().into_string().as_str(),
                        ));
                        let path2 = path.clone();
                        println!(
                            "Path to open : {}",
                            &path2.into_os_string().into_string().as_ref().unwrap()
                        );

                        let mut dialog = FileDialog::open_file(Some(path));
                        dialog.open();
                        dialog = dialog.anchor(Align2::CENTER_CENTER, Vec2::new(0.0, 0.0));
                        dialog_info.dialog = Some(dialog);
                    }
                });
            });

            ui.horizontal(|ui| {
                rendering::add_slider(ui, "Row index", &mut target_tile.row_index, 0..=target_tile.row_count - 1);
                rendering::add_slider(ui, "Col index", &mut target_tile.col_index, 0..=target_tile.col_count - 1);
            });
            

            ui.horizontal(|ui| {
                rendering::add_slider(ui, "Row count", &mut target_tile.row_count, 1..=16);
                rendering::add_slider(ui, "Col count", &mut target_tile.col_count, 1..=16);
            });

            ui.horizontal(|ui| {
                if target_tile.row_index > target_tile.row_count {
                    target_tile.row_index = target_tile.row_count - 1;
                }
                if target_tile.col_index > target_tile.col_count {
                    target_tile.col_index = target_tile.col_count - 1;
                }

                rendering::add_slider(ui, "Row span", &mut target_tile.row_span, 1..=target_tile.row_count - target_tile.row_index);
                rendering::add_slider(ui, "Col span", &mut target_tile.col_span, 1..=target_tile.col_count - target_tile.col_index);
            });

        });

        rendering::render_preview(ui, &target_tile, &filename);

        ui.vertical(|ui| {
            let save_btn = ui.add(Button::new("Save".to_string()));
            if save_btn.clicked() {
                // Tile Config validation
                let mut is_config_valid = true;
                if !my_app.is_edit
                    && my_app.is_id_already_used(my_app.cur_tile_config.as_ref().unwrap().id)
                {
                    my_app.toasts.add(egui_toast::Toast {
                        text: "ID already exists for another config tile".into(),
                        kind: egui_toast::ToastKind::Error,
                        options: egui_toast::ToastOptions::default()
                            .duration_in_seconds(2.0)
                            .show_progress(true),
                    });
                    is_config_valid = false;
                } else {
                }
                if is_config_valid
                    && my_app.is_name_already_used(
                        my_app.cur_tile_config.as_ref().unwrap().id,
                        my_app.cur_name_tile_config.as_ref().unwrap().clone(),
                    )
                {
                    my_app.toasts.add(egui_toast::Toast {
                        text: "The key name for the tile config is already used".into(),
                        kind: egui_toast::ToastKind::Error,
                        options: egui_toast::ToastOptions::default()
                            .duration_in_seconds(2.0)
                            .show_progress(true),
                    });
                    is_config_valid = false;
                }

                if is_config_valid {
                    optional_ret = Some(true);
                }
            }

            let cancel_btn = ui.add(Button::new("Cancel".to_string()));
            if cancel_btn.clicked() {
                // TODO:
                //  tile_configs
                optional_ret = Some(false);
            }
        });
    });

    let dialog_info = &mut my_app.open_dialog2;
    if let Some(dialog) = &mut dialog_info.dialog {
        if dialog.show(ui.ctx()).selected() {
            if let Some(file) = dialog.path() {
                dialog_info.path = Some(file.to_path_buf());
            }
        }
    }
    optional_ret
}
