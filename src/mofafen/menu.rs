use std::path::PathBuf;

use egui::{Separator, Vec2};
use egui_file::FileDialog;
use emath::align;
use super::types;
use super::enums;

// Render a top menu bar 
pub fn render_menu(ui: &mut egui::Ui, my_app : &mut types::MyApp ) {
    use egui::menu;

    menu::bar(ui, |ui| {
        
        /***
         * FILE Menu
         */
        ui.menu_button("File", |ui| {

            if ui.button("Open").clicked() {
                let path_buf :Option<PathBuf> =  my_app.inner_config.cur_working_dir.clone();
                let mut dialog = FileDialog::open_file(path_buf);
                dialog.open();
                dialog = dialog.anchor(emath::align::Align2::CENTER_CENTER, Vec2::new(0.0, 0.0));
                my_app.open_file_dialog = Some(dialog);

            }

            let menu_btn_save = ui.button("Save As");
            if menu_btn_save.clicked() {
                let path_buf :Option<PathBuf> = my_app.inner_config.cur_working_dir.clone();
                let mut dialog = FileDialog::save_file(path_buf);
                dialog.open();
                dialog = dialog.anchor(emath::align::Align2::CENTER_CENTER, Vec2::new(0.0, 0.0));
                my_app.save_file_dialog = Some(dialog);   
            }

            let menu_btn_working_dir = ui.button("Change working directory");
            if menu_btn_working_dir.clicked() {
                let path_buf :Option<PathBuf> = None;
                let mut dialog = FileDialog::select_folder(path_buf);
                dialog.open();
                dialog = dialog.anchor(align::Align2::CENTER_CENTER, Vec2::new(0.0, 0.0));
                my_app.change_wd_file_dialog = Some(dialog); 
            }

        });

        if let Some(dialog) = &mut my_app.open_file_dialog {

            if dialog.show(ui.ctx()).selected() {
                if let Some(file) = dialog.path() {
                    my_app.opened_file = Some(file.to_path_buf());
                    
                }
            }
        }

        if let Some(dialog) = &mut my_app.change_wd_file_dialog {
            if dialog.show(ui.ctx()).selected() {
                if let Some(file) = dialog.path() {
                    my_app.inner_config.cur_working_dir = Some(file.to_path_buf());
                }
            }
        }

        if let Some(dialog) = &mut my_app.save_file_dialog {
            if dialog.show(ui.ctx()).selected() {
                if let Some(file) = dialog.path() {
                    my_app.saved_file = Some(file.to_path_buf());
                }
            }
        }
        /***
        * VIEW Menu
        */
        ui.menu_button("View", |ui| {

            let label = match my_app.editor_mode_enum {
                enums::EditorMode::TileConfig => String::from("Go to Scene"),
                enums::EditorMode::Scene => String::from("Go to Tile Config")
            };

            let res: egui::Response = ui.button(label);
            if res.clicked() {
                my_app.editor_mode_enum = match my_app.editor_mode_enum {
                    enums::EditorMode::TileConfig => enums::EditorMode::Scene,
                    enums::EditorMode::Scene => enums::EditorMode::TileConfig
                };
            }
            
        });

        /***
        * HELP Menu
        */
        ui.menu_button("Help", |ui| {
            let res: egui::Response = ui.button("Version");

            let popup_id = ui.make_persistent_id("menu_id");
            let below = egui::AboveOrBelow::Below;
            egui::popup::popup_above_or_below_widget(ui, popup_id, &res, below, |ui| {
                ui.set_min_width(200.0); // if you want to control the size
                ui.label("Mofafen config editor - Alpha");
                ui.add(Separator::default());
                ui.label("Version : 0.0.1");
                ui.add(Separator::default());

            });

            if res.clicked() {
                ui.memory_mut(|mem| mem.toggle_popup(popup_id));
            }
            if my_app.inner_config.cur_working_dir.is_some(){
                let mut str = String::from("Current directory : ");
                str.push_str(my_app.inner_config.cur_working_dir.as_ref().unwrap().to_str().unwrap());
                ui.label(str );
            }
            
        });
    });
}