#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
use std::env;
use std::path::PathBuf;

use eframe::egui;
use egui::TopBottomPanel;
mod encoder;
mod mofafen;

fn main() -> Result<(), eframe::Error> {
    env_logger::init(); // Log to stderr (if you run with `RUST_LOG=debug`).
    let options = eframe::NativeOptions {
        viewport : egui::ViewportBuilder::default().with_inner_size([600.0, 800.0]),
        ..Default::default()
    };
    eframe::run_native(
        "Mofafen Configuration Editor",
        options,
        Box::new(|cc| {
            // Support for image loader
            egui_extras::install_image_loaders(&cc.egui_ctx);
            Box::<mofafen::types::MyApp>::default()
        }),
    )
}

impl eframe::App for mofafen::types::MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        if self.opened_file.is_some() {
            let res = self.opened_file.clone().unwrap();
            self.open(&res);
        }

        if self.saved_file.is_some() {
            let res = self.saved_file.clone().unwrap();
            self.save_as(&res);
        }

        if self.open_dialog2.path.is_some() {
            let mut path_prefix = env::current_dir().unwrap();
            path_prefix.push(String::from("assets"));

            let mut res: PathBuf = self.open_dialog2.path.clone().unwrap();
            if self.cur_tile_config.is_some() {
                match res.strip_prefix(path_prefix) {
                    Err(_) => {
                        self.toasts.add(egui_toast::Toast {
                            text: "Couldn't change filename".into(),
                            kind: egui_toast::ToastKind::Error,
                            options: egui_toast::ToastOptions::default()
                                .duration_in_seconds(2.0)
                                .show_progress(true),
                        });
                    }
                    Ok(r) => {
                        res = r.to_path_buf();
                        let cur_tile_config = self.cur_tile_config.as_mut();
                        let new_path = res.into_os_string().into_string().unwrap().clone();
                        cur_tile_config.unwrap().path = new_path;
                    }
                }
            }
        }

        render_header(ctx, self);

        egui::CentralPanel::default().show(ctx, |ui| {
            // Draw main component
            match self.editor_mode_enum {
                mofafen::enums::EditorMode::TileConfig => mofafen::tile_config::main_view(self, ui),
                mofafen::enums::EditorMode::Scene => mofafen::scene::main_view(self, ui)
            };
        });

        render_footer(ctx);
        self.toasts.show(ctx);
    }
}

/**
 * Global header, containing menu items
 */
fn render_header(ctx: &egui::Context, my_app: &mut mofafen::types::MyApp) {
    TopBottomPanel::top("header").show(ctx, |ui| {
        mofafen::menu::render_menu(ui, my_app);
        ui.heading("Configuration");
    });
}

/**
 * Footer
 */
fn render_footer(ctx: &egui::Context) {
    TopBottomPanel::bottom("footer").show(ctx, |ui| {
        ui.vertical_centered(|ui| {
            ui.heading("Footer");
        });
    });
}
